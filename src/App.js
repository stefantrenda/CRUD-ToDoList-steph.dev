import React, { useEffect, useState } from 'react';
import AddTaskForm from './components/AddTaskForm'
import UpdateForm from './components/UpdateForm'
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleCheck, faPen, faTrashCan } from '@fortawesome/free-solid-svg-icons'
import './App.css';

function App() {
  const [ToDo, setToDo] = useState([])
  const [newTask, setNewTask] = useState('');
  const [updateData, setUpdateData] = useState('');

  useEffect(() => {
    fetch(`http://jsonplaceholder.typicode.com/todos`)
      .then(res => res.json())
      .then(data => {
        setToDo(data)
      })
  }, [])


  const addTask = () => {
    if (newTask) {
      let num = ToDo.length + 1;
      let newEntry = { id: num, title: newTask, status: false }
      setToDo([...ToDo, newEntry])
      setNewTask('')
    }
  }

  const deleteTask = (id) => {
    let newTasks = ToDo.filter(task => task.id !== id)
    setToDo(newTasks)
  }

  const markDone = (id) => {
    let newTask = ToDo.map(task => {
      if (task.id === id) {
        return ({ ...task, status: !task.status })
      }
      return task
    })
    setToDo(newTask);
  }

  const cancelUpdate = (id) => {
    setUpdateData('')
  }
  const changeTask = (e) => {
    let newEntry = {
      id: updateData.id,
      title: e.target.value,
      status: updateData.status ? true : false
    }
    setUpdateData(newEntry);
  }

  const updateTask = () => {
    let filterRecords = [...ToDo].filter(task => task.id !== updateData.id);
    let updatedObject = [...filterRecords, updateData]
    setToDo(updatedObject);
    setUpdateData('')
  }



  return (
    <div className="container App">

      <br /><br />
      <h2>To Do List</h2>
      <br /><br />
      {/* {Update Task} */}

      {updateData && updateData ? (
        <UpdateForm
          updateData={updateData}
          changeTask={changeTask}
          updateTask={updateTask}
          cancelUpdate={cancelUpdate}
        />


      ) : (
        <AddTaskForm
          newTask={newTask}
          setNewTask={setNewTask}
          addTask={addTask}
        />
      )}



      {/* {Display ToDos} */}

      {ToDo && ToDo.length ? '' : 'No Tasks...'}

      {ToDo && ToDo
        .sort((a, b) => a.id > b.id ? 1 : -1)
        .map((task, index) => {
          return (
            <React.Fragment key={task.id}>

              <div className='col taskBg'>

                <div className={task.status ? 'done' : ''}>
                  <span className="taskNumber">{index + 1}</span>
                  <span className="taskText">{task.title}</span>
                </div>

                <div className='iconWrap'>

                  <span
                    title='Completed / Not Completed'
                    onClick={(e) => markDone(task.id)}><FontAwesomeIcon icon={faCircleCheck} /></span>

                  {task.status ? null : (
                    <span title='Edit'
                      onClick={() => setUpdateData({
                        id: task.id,
                        title: task.title,
                        status: task.status ? true : false
                      })}
                    ><FontAwesomeIcon icon={faPen} /></span>
                  )}

                  <span
                    onClick={() => deleteTask(task.id)}
                    title='Delete'><FontAwesomeIcon icon={faTrashCan} /></span>
                </div>
              </div>
            </React.Fragment>
          )
        })
      }

    </div>
  );
}

export default App;
